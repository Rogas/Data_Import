'''
This program should be executed after the dataParse execution is done.
Before running this program, please execute the following statements in PostgreSQL to create tables.
This program is to parse the relationships of the ACM Bibliographical network.
--------------------------------------------------------------
create table writes(
AUid int not null,
ARid int not null,
primary key (AUid, ARid),
foreign key (AUid) references AUTHOR (AUid),
foreign key (ARid) references ARTICLE (ARid));

create table cites(
ARid int not null,
CitedARid int not null,
primary key (ARid, CitedARid),
foreign key (ARid) references ARTICLE (ARid),
foreign key (CitedARid) references ARTICLE (ARid));
--------------------------------------------------------------
@author: minjian
'''
import os
import xml.sax
import time
import psycopg2

#This class is to handle all files of the periodical directory and the proceeding directory 
#(i.e. every relationship records)
class ACMRelationshipHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.currentData = ""
        self.writes = set()
        self.references = set()
        self.citation = set()
        
        # For Author
        self.auid = "null"
        
        # For Article
        self.arid = "null"
        self.refObjId = "null"
        self.citedObjId = "null"
    
    def startElement(self, name, attrs):
        self.currentData = name
        if (self.currentData == "article_id"):
            self.arid = "null"
            self.refObjId = "null"
            self.citedObjId = "null" 
            self.auid = "null"           
            
    def endDocument(self):
        global failedReason
        global cur, conn
        
        print "--------start insert data--------"
        
        if len(self.writes) != 0:
            for eachWrites in self.writes:
                try:
                    cur.execute("INSERT INTO writes VALUES(%s, %s)" % (eachWrites[1], eachWrites[0]))
                    conn.commit()
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()
            self.writes.clear()                    
        if len(self.references) != 0:
            for eachRef in self.references:
                try:
                    cur.execute("INSERT INTO cites VALUES(%s, %s)" % (eachRef[0], eachRef[1]))
                    conn.commit()
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()
            self.references.clear()                    
        if len(self.citation) != 0:
            for eachCite in self.citation:
                try:
                    cur.execute("INSERT INTO cites VALUES(%s, %s)" % (eachCite[1], eachCite[0]))
                    conn.commit()
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor() 
            self.citation.clear()                   
        '''
        for debug
        for eachWrites in self.writes:
            print eachWrites[0], " is written by ",eachWrites[1]
        for eachRef in self.references:
            print eachRef[0], " refer to ", eachRef[1]
        for eachCite in self.citation:
            print eachCite[0], " is cited by ", eachCite[1]
        '''       
    def endElement(self, name):
        
        # Last useful element of author record is email_address   
        if (self.currentData == "last_name") and (self.auid != "null") and (self.arid != "null"):
            self.writes.add((self.arid, self.auid))
            self.auid = "null"
            
        #Last useful element of ref is ref_id
        elif (self.currentData == "ref_id") and (self.refObjId != "null") and (self.arid != "null"):
            self.references.add((self.arid, self.refObjId))
            self.refObjId = "null"
            
        #Last useful element of cited_by is cited_by_text
        elif (self.currentData == "cited_by_text") and (self.citedObjId != "null") and (self.arid != "null"):
            self.citation.add((self.arid, self.citedObjId))
            self.citedObjId = "null"
            
        self.currentData = ""
        
    def characters(self, content):
        # For Article
        if self.currentData == "article_id" and content.isnumeric():
            self.arid = self.getArticleID(int(content))
            
        # For Author
        elif self.currentData == "person_id" and content.isalnum():
            self.auid = self.getAuthorID("'" + content + "'")
        
        # For References
        elif self.currentData == "ref_obj_id" and content.isnumeric():
            self.refObjId = self.getArticleID(int(content))
        
        # For Citation
        elif self.currentData == "cited_by_object_id" and content.isnumeric():
            self.citedObjId = self.getArticleID(int(content))
            
    def getAuthorID (self, personid):
        global conn, cur
        cur.execute("select auid from author where personid = %s;" % (personid))
        rows = cur.fetchall()
        if len(rows) != 0 and len(rows) == 1:        # all rows in table
            auid = rows[0][0]
        else: 
            auid = "null"
        conn.commit()
        return auid

    def getArticleID (self, articleid):
        global conn, cur
        cur.execute("select arid from article where articleid = %s;" % (articleid))
        rows = cur.fetchall()
        if len(rows) != 0:        # all rows in table
            arid = rows[0][0]
        else: 
            arid = "null"
        conn.commit()
        return arid    

#Here is connect to your PostgreSQL
#Change you database, user and port here          
db = "acm"
dbUser = "minjian"
dbPort = 5433
      
conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
cur = conn.cursor()
'''
#for debug
f = open("/home/minjian/Documents/proceeding/PROC--1971-800024.xml")
xml.sax.parse(f, ACMRelationshipHandler())

f.close()
cur.close()
conn.close()
'''

failedReason = set()
# start periodical data
#count = 0
#Here is the directory of the journal files
os.chdir("/home/minjian/Documents/periodical")
fileList = os.listdir('.')
#fileList = [1] #for debug
for eachPeriodical in fileList:
    #count += 1
    print eachPeriodical
    f1 = open(eachPeriodical)
    #f1 = open("MAG-CACM-V56I5-2447976.xml") #for debug
    before_time = time.time()
    try:
        xml.sax.parse(f1, ACMRelationshipHandler())
        #xml.sax.parse(f1, testParseHandler())
    except xml.sax._exceptions.SAXParseException as reason:
        failedReason.add(reason)
        continue
        
    f1.close()
    #if count == 2:
        #break

#start proceeding data
#count = 0
#Here is the directory of the proceeding files
os.chdir("/home/minjian/Documents/proceeding")
fileList = os.listdir('.')
for eachProceeding in fileList:
    #count += 1
    print eachProceeding
    f2 = open(eachProceeding)
    before_time = time.time()
    try:
        xml.sax.parse(f2, ACMRelationshipHandler())
        #xml.sax.parse(f2, testParseHandler())
    except xml.sax._exceptions.SAXParseException as reason:
        failedReason.add(reason)
        continue
    f2.close()
    #if count == 2:
        #break
cur.close()
conn.close()
print "-----------------"    
print len(failedReason)

for eachReason in failedReason:
    print eachReason