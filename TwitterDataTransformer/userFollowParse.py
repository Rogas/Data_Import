'''
Before running this program, please execute the following statements in PostgreSQL to create tables.
This program is to parse the "User" entity and "Follow" relationship of the Twitter network.
--------------------------------------------------------------
create table tweet_user(
Uid int not null,
Display_name text,
primary key (Uid));

create table follow(
Uid int not null,
Follower_id int not null,
primary key (Uid, Follower_id),
foreign key (Uid) references tw_user (Uid),
foreign key (Follower_id) references tw_user (Uid));

--------------------------------------------------------------
This statement is to extract all tw_users who appear in the tweet raw data file (Optional).

create table tweet_user as 
(select uid, display_name from tw_user where uid in 
(select distinct owner_id from 
(select t.owner_id from tweet as t union select m.uid from mentioned_in as m) as union_user));
'''
import psycopg2
import time

#Here is connect to your PostgreSQL
#Change you database, user and port here 
db = "fulltwitter"
dbUser = "minjian"
dbPort = 5433
      
conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
cur = conn.cursor()
userIDSet = set()

#Here starts to parse userID and display_name
f1 = open("/home/minjian/Documents/numeric2screen")
user_time = time.time()
for eachline in f1:
    #count += 1
    data = eachline.split()
    userId = int(data[0])
    userName = "'" + data[1] + "'"
    
    userIDSet.add(userId)
    cur.execute("INSERT INTO tw_user VALUES(%s, %s)" % (userId, userName))
    conn.commit()

print "Total Tw_user Data Transfer time is: ", (time.time() - user_time)/60
f1.close()

#Here starts to parse the follow relationship
f2 = open("/media/minjian/OSDisk/twitter_rv.txt")
follow_time = time.time()
count = 0
for eachline in f2:
    data = eachline.split()
    userId = int(data[0])
    followerId = int(data[1])
    if ((userId in userIDSet) and (followerId in userIDSet)):
        cur.execute("INSERT INTO follow VALUES(%s, %s)" % (userId, followerId))
        #print userId,"has follower", followerId
        count += 1
        if count == 50:
            conn.commit()
            count = 0
            
conn.commit()
print "Total Data Transfer time is: ", (time.time() - follow_time)/60
cur.close()
conn.close()
f2.close()