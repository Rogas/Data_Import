'''
This program should be executed after the userFollowParse execution is done.
Before running this program, please execute the following statements in PostgreSQL to create tables.
This program is to parse the entities of the ACM Bibliographical network.
--------------------------------------------------------------

create table tweet(
TWid int not null,
Owner_id int not null,
Tweet_time text,
primary key (TWid),
foreign key (Owner_id) references tw_user (Uid));

create table tag(
Tid int not null,
Tag_label text,
primary key (Tid));

create table labelled_by(
TWid int not null,
Tid int not null,
primary key (TWid, Tid),
foreign key (Tid) references tag (Tid),
foreign key (TWid) references tweet (TWid));

create table mentioned_in(
Uid int not null,
TWid int not null,
primary key (Uid, TWid),
foreign key (Uid) references tw_user (Uid),
foreign key (TWid) references tweet (TWid));
'''
import psycopg2
import time

def getUser ():
    global conn, cur, failedReason, userAndID
    cur.execute("select * from tw_user;" )
    rows = cur.fetchall()
    for eachRecord in rows:
        userAndID[eachRecord[1]] = eachRecord[0]
    conn.commit()

#Here is connect to your PostgreSQL
#Change you database, user and port here 
db = "fulltwitter"
dbUser = "minjian"
dbPort = 5433
      
conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
cur = conn.cursor()

cur.execute("select max(uid) from tw_user;")
rows = cur.fetchall()  # all rows in table
maxUserId = rows[0][0]
conn.commit()

print "start to get user"
userAndID = dict()
getUser()
print "user got"

#Here is the directory of the tweet raw data file
f = open("/home/minjian/Documents/tweets2009-06.txt")

tweetCount = 0
mentionCount = 0
labelCount =0

count = 0
tagSet = set()
failedReason = set()
tagAndID = dict()
sysAssID = maxUserId + 1
rawData = []
before_time = time.time()
for eachline in f:
    count += 1
    rawData.append(eachline)
    if count == 4:
        if ("No Post Title" not in rawData[3]):
            tweetTime = "'" + (rawData[1]).strip("T").strip() + "'"
            userURL = (rawData[2]).strip("U").strip()
            tweetOwner = userURL[userURL.rindex("/") + 1 :]
            text = (rawData[3]).strip("W").strip();
            #ownerID = getUserID(tweetOwner)
            
            if tweetOwner in userAndID:
                sysAssID += 1
                tweetCount += 1
                twID = sysAssID
                cur.execute("INSERT INTO tweet VALUES(%s, %s, %s)" % (twID, userAndID[tweetOwner], tweetTime))
                conn.commit()
                
                #Here is for mentioned_in
                if ("@" in text):
                    pIndex = 0
                    while text.find("@", pIndex) != -1:
                        mentionCount += 1
                        atIndex = text.index("@", pIndex)
                        nextAtSpaceIndex = text.find(" ", atIndex)
                        quoteAtSpaceIndex = text.find("'", atIndex)
                        #print nextAtSpaceIndex
                        #deal with special situation: @Healesville's Twitter
                        if(quoteAtSpaceIndex != -1 and quoteAtSpaceIndex < nextAtSpaceIndex):
                            name = text[atIndex + 1 : quoteAtSpaceIndex]
                            if name in userAndID:
                                try:
                                    cur.execute("INSERT INTO mentioned_in VALUES(%s, %s)" % (userAndID[name], twID))
                                    conn.commit()
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                               
                                
                        #deal with special situation: @Healesville's?\n
                        elif (nextAtSpaceIndex == -1 and quoteAtSpaceIndex != -1):
                            name = text[atIndex + 1 : quoteAtSpaceIndex]
                            if name in userAndID:
                                try:
                                    cur.execute("INSERT INTO mentioned_in VALUES(%s, %s)" % (userAndID[name], twID))
                                    conn.commit()                       
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                        
                        else:
                            name =text[atIndex + 1 : nextAtSpaceIndex]
                            if name in userAndID:
                                try:
                                    cur.execute("INSERT INTO mentioned_in VALUES(%s, %s)" % (userAndID[name], twID))
                                    conn.commit()                       
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor() 
                        pIndex = atIndex + 1                
                
                #Here is for labelled_by   
                if ("#" in text):
                    pIndex = 0
                    while text.find("#", pIndex) != -1:
                        labelCount += 1               
                        hashIndex = text.index("#", pIndex)
                        nextHashSpaceIndex = text.find(" ", hashIndex)
                        quoteHashSpaceIndex = text.find("'", hashIndex)                    
                        #deal with special situation: #HealesvilleSpringFestival's Twitter
                        if (quoteHashSpaceIndex != -1 and quoteHashSpaceIndex < nextHashSpaceIndex):
                            tagLabel = "'" + text[hashIndex + 1 : quoteHashSpaceIndex] + "'"
                            if tagLabel not in tagSet:
                                sysAssID += 1
                                tagID = sysAssID
                                try:
                                    cur.execute("INSERT INTO tag VALUES(%s, %s)" % (tagID, tagLabel))
                                    conn.commit() 
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagID))
                                    conn.commit()
                                    tagAndID[tagLabel] = tagID
                                    tagSet.add(tagLabel)
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                                    
                            else:
                                try:
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagAndID[tagLabel]))
                                    conn.commit()
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                                                                
                        #deal with special situation: #HealesvilleSpringFestival's?\n
                        elif (nextHashSpaceIndex == -1 and quoteHashSpaceIndex != -1):
                            tagLabel = "'" + text[hashIndex + 1 : quoteHashSpaceIndex] + "'"
                            if tagLabel not in tagSet:
                                sysAssID += 1
                                tagID = sysAssID
                                try:
                                    cur.execute("INSERT INTO tag VALUES(%s, %s)" % (tagID, tagLabel))
                                    conn.commit() 
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagID))
                                    conn.commit()
                                    tagAndID[tagLabel] = tagID
                                    tagSet.add(tagLabel)
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                                         
                            else:
                                try:
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagAndID[tagLabel]))
                                    conn.commit() 
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                                   
                        else:
                            tagLabel = "'" + text[hashIndex + 1 : nextHashSpaceIndex] + "'"
                            if tagLabel not in tagSet:
                                sysAssID += 1
                                tagID = sysAssID
                                try:
                                    cur.execute("INSERT INTO tag VALUES(%s, %s)" % (tagID, tagLabel))
                                    conn.commit() 
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagID))
                                    conn.commit()
                                    tagAndID[tagLabel] = tagID
                                    tagSet.add(tagLabel)
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()                                        
                            else:
                                try:
                                    cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" % (twID, tagAndID[tagLabel]))
                                    conn.commit() 
                                except psycopg2.Error as reason:
                                    failedReason.add(reason)
                                    cur.close()
                                    conn.close()
                                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                                    cur = conn.cursor()  
                        pIndex = hashIndex + 1                               
            
        count = 0
        rawData = []
print "Total Data Transfer time is: ", (time.time() - before_time)/60
cur.close()
conn.close()
f.close()
