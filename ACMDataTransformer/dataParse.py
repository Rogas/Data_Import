'''
Before running this program, please execute the following statements in PostgreSQL to create tables.
This program is to parse the entities of the ACM Bibliographical network.
--------------------------------------------------------------
set datestyle to MDY;

create table author(
AUid int not null primary key,
PersonId text not null,
AuthorProfileID bigint, 
FName text,
MName text,
LName text,
Affiliation text,
Email text);

create table publisher(
PUid int not null primary key,
PublisherId text not null,
Name text,
ZipCode text,
City text,
State text,
Country text);

create table journal(
JOid int not null primary key,
JournalId text not null,
Name text,
Periodical_Type text,
Publication_Date date,
PUid int,
foreign key (PUid) references publisher (PUid));

create table proceeding(
PRid int not null primary key,
ProceedingId bigint not null,
Title text,
Subtitle text,
Proc_Desc text,
Con_City text,
Con_State text,
Con_Country text,
Con_Start_Date date,
Con_End_Date date,
Publication_Date date,
PUid int,
foreign key (PUid) references publisher (PUid));

create table article(
ARid int not null primary key,
ArticleId bigint not null,
Title text,  
Publication_Date date,
JOid int,
PRid int,
foreign key (JOid) references journal (JOid),
foreign key (PRid) references proceeding (PRid));
--------------------------------------------------------------
@author: minjian
'''
import os
import xml.sax
import time
import psycopg2

#This class is to handle all files of the periodical directory 
#(i.e. every entity records that are related with journal)
class ACMPeriodicalHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.currentData = ""
        self.authors = []
        self.authorData = []
        self.articles = []
        self.articleData = []
        self.journalData = []
        self.publisherData = []
        self.tempPublicationDate = "null"
        
        # For Author
        self.personId = "null"
        self.profileId = "null"
        self.firstName = "null"
        self.middleName = "null"
        self.lastName = "null"
        self.affiliation = "null"
        self.email = "null"
        
        # For Article
        self.articleId = "null"
        self.articleTitle = "null"
        self.articlePublicationDate = "null"
        
        # For Journal
        self.journalId = "null"
        self.journalName = "null"
        self.periodicalType = "null"
        self.journalPublicationDate = "null"

        # For Publisher
        self.publisherId = "null"
        self.publisherName = "null"
        self.publisherZipCode = "null"
        self.publisherCity = "null"
        self.publisherState = "null"
        self.publisherCountry = "null"
        
    def clearAuthor(self):
        # For Author
        self.personId = "null"
        self.profileId = "null"
        self.firstName = "null"
        self.middleName = "null"
        self.lastName = "null"
        self.affiliation = "null"
        self.email = "null"        
    
    def clearArticle(self):
        # For Article
        self.articleId = "null"
        self.articleTitle = "null"
        self.articlePublicationDate = "null"
            
    def startElement(self, name, attrs):
        self.currentData = name
            
    def endDocument(self):
        global journalIdSet
        global publisherIdSet
        global sysAssId
        global failedReason
        global cur, conn
        
        if self.journalId not in journalIdSet:
            journalIdSet.add(self.journalId)
            sysAssId += 1
            self.journalData.append(sysAssId)
            self.journalData.append(self.journalId)
            self.journalData.append(self.journalName)
            self.journalData.append(self.periodicalType)
            self.journalData.append(self.journalPublicationDate)
        
        if self.publisherId not in publisherIdSet:
            publisherIdSet.add(self.publisherId)
            sysAssId += 1
            self.publisherData.append(sysAssId)
            self.publisherData.append(self.publisherId)
            self.publisherData.append(self.publisherName)
            self.publisherData.append(self.publisherZipCode)
            self.publisherData.append(self.publisherCity)
            self.publisherData.append(self.publisherState)
            self.publisherData.append(self.publisherCountry)

        print "--------start insert data--------"
        #print self.journalData
        #print self.publisherData
        #for eachArt in self.articles:
            #print eachArt
        #for eachAu in self.authors:
            #print eachAu
        
        if len(self.publisherData) != 0:
            # print "publisherData exist!!!"
            cur.execute("INSERT INTO publisher VALUES(%s, %s, %s, %s, %s, %s, %s)" % 
                        (self.publisherData[0], self.publisherData[1], self.publisherData[2], 
                         self.publisherData[3], self.publisherData[4], self.publisherData[5], self.publisherData[6]))
            conn.commit()
        
        if len(self.journalData) != 0:
            if len(self.publisherData) != 0:
                cur.execute("INSERT INTO journal VALUES(%s, %s, %s, %s, %s, %s)" % 
                            (self.journalData[0], self.journalData[1], self.journalData[2], 
                             self.journalData[3], self.journalData[4], self.publisherData[0]))
                conn.commit()
                
            else:
                cur.execute("SELECT puid from publisher where puid = %s" % (self.publisherId))
                rows = cur.fetchall()
                cur.execute("INSERT INTO journal VALUES(%s, %s, %s, %s, %s, %s)" % 
                            (self.journalData[0], self.journalData[1], self.journalData[2], 
                             self.journalData[3], self.journalData[4], rows[0][0]))
                conn.commit()
        
        for eachAuthor in self.authors:
            if len(eachAuthor) != 0:
                cur.execute("INSERT INTO author VALUES(%s, %s, %s, %s, %s, %s, %s, %s)" % 
                            (eachAuthor[0], eachAuthor[1], eachAuthor[2], eachAuthor[3], 
                             eachAuthor[4], eachAuthor[5], eachAuthor[6], eachAuthor[7]))
                conn.commit()
                
        #for some unknown reasons, xml.sax only parse year of publication date for some records
        #as for the records in the same file, their publication dates are the same.
        #so here once we successfully get the complete publication date, we use it for all records in this file
        #otherwise we use "11-11-1111" as the temp publication date (after running this program, no temp publication date in the database)
        if self.tempPublicationDate == "null":
            for article in self.articles:
                if article[3].count("-") == 2:
                    self.tempPublicationDate = article[3]
                    break
                else:
                    self.tempPublicationDate = "'11-11-1111'"
                      
        for eachArticle in self.articles:
            #There is a new journal in the file
            if len(self.journalData) != 0:
                try:
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 eachArticle[3], self.journalData[0], "null"))
                    conn.commit()                            
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 self.tempPublicationDate, self.journalData[0], "null"))
                    conn.commit()     
            #There is an old journal in the file
            #Need to get journal info from the database           
            else:
                #can be more efficient
                cur.execute("SELECT joid from journal where joid = %s" % (self.journalId))
                rows = cur.fetchall()
                try:
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 eachArticle[3], rows[0][0], "null"))
                    conn.commit()                            
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()                    
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 self.tempPublicationDate, rows[0][0], "null"))
                    conn.commit()   
                
    def endElement(self, name):
        global articleIdSet
        global authorIdSet
        global sysAssId
        # Last useful element of article record is title
        if (self.currentData == "title") and (self.articleId not in articleIdSet):
            articleIdSet.add(self.articleId)
            sysAssId += 1
            self.articleData.append(sysAssId)            
            self.articleData.append(self.articleId)
            self.articleData.append(self.articleTitle)
            self.articleData.append(self.articlePublicationDate)
            self.articles.append(self.articleData)
            self.articleData = []
            self.clearArticle()
        
        # Last useful element of author record is email_address   
        elif (self.currentData == "email_address") and (self.personId not in authorIdSet):
            authorIdSet.add(self.personId)
            sysAssId += 1
            self.authorData.append(sysAssId)
            self.authorData.append(self.personId)
            self.authorData.append(self.profileId)
            self.authorData.append(self.firstName)
            self.authorData.append(self.middleName)
            self.authorData.append(self.lastName)
            self.authorData.append(self.affiliation)
            self.authorData.append(self.email)
            self.authors.append(self.authorData)
            self.authorData = []
            self.clearAuthor()
            
        self.currentData = ""
        
    def characters(self, content):
        # For Article
        if self.currentData == "article_id":
            self.articleId = int(content)
        elif self.currentData == "title":
            if "'" in content:
                content = content.replace("'", "''")
            self.articleTitle = "'" + content + "'"
        elif self.currentData == "article_publication_date":
            self.articlePublicationDate = "'" + content + "'"
            
        # For Author
        elif self.currentData == "person_id":
            self.personId = "'" + content + "'"
        elif self.currentData == "author_profile_id":
            self.profileId = int(content)
        elif self.currentData == "first_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.firstName = "'" + content + "'"
        elif self.currentData == "middle_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.middleName = "'" + content+ "'"
        elif self.currentData == "last_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.lastName = "'" + content + "'"
        elif self.currentData == "affiliation":
            if "'" in content:
                content = content.replace("'", "''")            
            self.affiliation = "'" + content + "'"
        elif self.currentData == "email_address":
            if "'" in content:
                content = content.replace("'", "''")            
            self.email = "'" + content + "'"
        
        # For Journal
        elif self.currentData == "journal_id":
            self.journalId = "'" + content + "'"
        elif self.currentData == "journal_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.journalName = "'" + content + "'"
        elif self.currentData == "periodical_type":
            if "'" in content:
                content = content.replace("'", "''")
            self.periodicalType = "'" + content + "'"
        elif self.currentData == "publication_date":
            self.journalPublicationDate = "'" + content + "'"
        
        # For Publisher
        elif self.currentData == "publisher_id":
            self.publisherId = "'" + content + "'"
        elif self.currentData == "publisher_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherName = "'" + content + "'"
        elif self.currentData == "publisher_zip_code":
            self.publisherZipCode = "'" + content + "'"
        elif self.currentData == "publisher_city":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherCity = "'" + content + "'"
        elif self.currentData == "publisher_state":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherState = "'" + content + "'"
        elif self.currentData == "publisher_country":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherCountry = "'" + content + "'"
            
#This class is to handle all files of the proceeding directory 
#(i.e. every records that are related with proceeding)
class ACMProceedingHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.currentData = ""
        self.authors = []
        self.authorData = []
        self.articles = []
        self.articleData = []
        self.proceedingData = []
        self.publisherData = []
        self.tempPublicationDate = "null"
        
        # For Author
        self.personId = "null"
        self.profileId = "null"
        self.firstName = "null"
        self.middleName = "null"
        self.lastName = "null"
        self.affiliation = "null"
        self.email = "null"
        
        # For Article
        self.articleId = "null"
        self.articleTitle = "null"
        self.articlePublicationDate = "null"
        
        # For Proceeding
        self.proceedingId = "null"
        self.proceedingTitle = "null"
        self.proceedingSubtitle = "null"
        self.proceedingDesc = "null"
        self.conferCity = "null"
        self.conferState = "null"
        self.conferCountry = "null"
        self.conferStartDate = "null"
        self.conferEndDate = "null"
        self.proceedingPublicationDate = "null"

        # For Publisher
        self.publisherId = "null"
        self.publisherName = "null"
        self.publisherZipCode = "null"
        self.publisherCity = "null"
        self.publisherState = "null"
        self.publisherCountry = "null"
    
    def clearAuthor(self):
        # For Author
        self.personId = "null"
        self.profileId = "null"
        self.firstName = "null"
        self.middleName = "null"
        self.lastName = "null"
        self.affiliation = "null"
        self.email = "null"        
    
    def clearArticle(self):
        # For Article
        self.articleId = "null"
        self.articleTitle = "null"
        self.articlePublicationDate = "null"        
    
    def startElement(self, name, attrs):
        self.currentData = name
            
    def endDocument(self):
        global proceedingIdSet
        global publisherIdSet
        global sysAssId
        global cur, conn
        
        if self.proceedingId not in proceedingIdSet:
            proceedingIdSet.add(self.proceedingId)
            sysAssId += 1
            self.proceedingData.append(sysAssId)
            self.proceedingData.append(self.proceedingId)
            self.proceedingData.append(self.proceedingTitle)
            self.proceedingData.append(self.proceedingSubtitle)
            self.proceedingData.append(self.proceedingDesc)
            self.proceedingData.append(self.conferCity)
            self.proceedingData.append(self.conferState)
            self.proceedingData.append(self.conferCountry)
            self.proceedingData.append(self.conferStartDate)
            self.proceedingData.append(self.conferEndDate)
            self.proceedingData.append(self.proceedingPublicationDate)
        
        if self.publisherId not in publisherIdSet:
            publisherIdSet.add(self.publisherId)
            sysAssId += 1
            self.publisherData.append(sysAssId)
            self.publisherData.append(self.publisherId)
            self.publisherData.append(self.publisherName)
            self.publisherData.append(self.publisherZipCode)
            self.publisherData.append(self.publisherCity)
            self.publisherData.append(self.publisherState)
            self.publisherData.append(self.publisherCountry)
        
        print "--------start insert data--------" 
        #print self.proceedingData
        #print self.publisherData
        #for eachArt in self.articles:
            #print eachArt
        #for eachAu in self.authors:
            #print eachAu

        if len(self.publisherData) != 0:
            # print "publisherData exist!!!"
            cur.execute("INSERT INTO publisher VALUES(%s, %s, %s, %s, %s, %s, %s)" % 
                        (self.publisherData[0], self.publisherData[1], self.publisherData[2], 
                         self.publisherData[3], self.publisherData[4], self.publisherData[5], self.publisherData[6]))
            conn.commit()
        
        if len(self.proceedingData) != 0:
            if len(self.publisherData) != 0:
                cur.execute("INSERT INTO proceeding VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" % 
                            (self.proceedingData[0], self.proceedingData[1], self.proceedingData[2], 
                             self.proceedingData[3], self.proceedingData[4], self.proceedingData[5], 
                             self.proceedingData[6], self.proceedingData[7], self.proceedingData[8],
                             self.proceedingData[9], self.proceedingData[10], self.publisherData[0]))
                conn.commit()
                
            else:
                cur.execute("SELECT puid from publisher where puid = %s" % (self.publisherId))
                rows = cur.fetchall()
                cur.execute("INSERT INTO proceeding VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" % 
                            (self.proceedingData[0], self.proceedingData[1], self.proceedingData[2], 
                             self.proceedingData[3], self.proceedingData[4], self.proceedingData[5], 
                             self.proceedingData[6], self.proceedingData[7], self.proceedingData[8],
                             self.proceedingData[9], self.proceedingData[10], rows[0][0] ))
                conn.commit()
        
        for eachAuthor in self.authors:
            if len(eachAuthor) != 0:
                cur.execute("INSERT INTO author VALUES(%s, %s, %s, %s, %s, %s, %s, %s)" % 
                            (eachAuthor[0], eachAuthor[1], eachAuthor[2], eachAuthor[3], 
                             eachAuthor[4], eachAuthor[5], eachAuthor[6], eachAuthor[7]))
                conn.commit()

        #for some unknown reasons xml.sax only parse year of publication date
        #here use the first file publication date to insert
        if self.tempPublicationDate == "null":
            for article in self.articles:
                if article[3].count("-") == 2:
                    self.tempPublicationDate = article[3]
                    break
                else:
                    self.tempPublicationDate = "'11-11-1111'"
                      
        for eachArticle in self.articles:
            if len(self.proceedingData) != 0:
                try:
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 eachArticle[3], "null", self.proceedingData[0]))
                    conn.commit()                           
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 self.tempPublicationDate, "null", self.proceedingData[0]))
                    conn.commit()               
            else:
                #can be more efficient
                cur.execute("SELECT prid from proceeding where prid = %s" % (self.proceedingId))
                rows = cur.fetchall()
                try:
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 eachArticle[3], "null", rows[0][0]))
                    conn.commit()                           
                except psycopg2.Error as reason:
                    failedReason.add(reason)
                    cur.close()
                    conn.close()
                    conn = conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                    cur = conn.cursor()                    
                    cur.execute("INSERT INTO article VALUES(%s, %s, %s, %s, %s, %s)" % 
                                (eachArticle[0], eachArticle[1], eachArticle[2], 
                                 self.tempPublicationDate, "null", rows[0][0]))
                    conn.commit()              
                
    def endElement(self, name):
        global articleIdSet
        global authorIdSet
        global sysAssId       
        # Last useful element of article record is title
        if (self.currentData == "title") and (self.articleId not in articleIdSet):
            articleIdSet.add(self.articleId)
            sysAssId += 1
            self.articleData.append(sysAssId)            
            self.articleData.append(self.articleId)
            self.articleData.append(self.articleTitle)
            self.articleData.append(self.articlePublicationDate)
            self.articles.append(self.articleData)
            self.articleData = []
            self.clearArticle()
        
        # Last useful element of author record is email_address   
        elif (self.currentData == "email_address") and (self.personId not in authorIdSet):
            authorIdSet.add(self.personId)
            sysAssId += 1
            self.authorData.append(sysAssId)
            self.authorData.append(self.personId)
            self.authorData.append(self.profileId)
            self.authorData.append(self.firstName)
            self.authorData.append(self.middleName)
            self.authorData.append(self.lastName)
            self.authorData.append(self.affiliation)
            self.authorData.append(self.email)
            self.authors.append(self.authorData)
            self.authorData = []
            self.clearAuthor()
            
        self.currentData = ""
        
    def characters(self, content):
        # For Article
        if self.currentData == "article_id":
            self.articleId = int(content)
        elif self.currentData == "title":
            if "'" in content:
                content = content.replace("'", "''")
            self.articleTitle = "'" + content + "'"
        elif self.currentData == "article_publication_date":
            self.articlePublicationDate = "'" + content + "'"
            
        # For Author
        elif self.currentData == "person_id":
            self.personId = "'" + content + "'"
        elif self.currentData == "author_profile_id":
            self.profileId = int(content)
        elif self.currentData == "first_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.firstName = "'" + content + "'"
        elif self.currentData == "middle_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.middleName = "'" + content+ "'"
        elif self.currentData == "last_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.lastName = "'" + content + "'"
        elif self.currentData == "affiliation":
            if "'" in content:
                content = content.replace("'", "''")
            self.affiliation = "'" + content + "'"
        elif self.currentData == "email_address":
            if "'" in content:
                content = content.replace("'", "''")
            self.email = "'" + content + "'"
        
        # For Proceeding
        elif self.currentData == "proc_id":
            self.proceedingId = "'" + content + "'"
        elif self.currentData == "proc_desc":
            if "'" in content:
                content = content.replace("'", "''")
            self.proceedingDesc = "'" + content + "'"
        elif self.currentData == "proc_title":
            if "'" in content:
                content = content.replace("'", "''")            
            self.proceedingTitle = "'" + content + "'"
        elif self.currentData == "proc_subtitle":
            if "'" in content:
                content = content.replace("'", "''")
            self.proceedingSubtitle = "'" + content + "'"
        elif self.currentData == "publication_date":
            self.proceedingPublicationDate = "'" + content + "'"
        elif self.currentData == "start_date":
            self.conferStartDate = "'" + content + "'"
        elif self.currentData == "end_date":
            self.conferEndDate = "'" + content + "'"
        elif self.currentData == "city":
            if "'" in content:
                content = content.replace("'", "''")
            self.conferCity = "'" + content + "'"
        elif self.currentData == "state":
            if "'" in content:
                content = content.replace("'", "''")
            self.conferState = "'" + content + "'"
        elif self.currentData == "country":
            if "'" in content:
                content = content.replace("'", "''")
            self.conferCountry = "'" + content + "'"
            
        
        # For Publisher
        elif self.currentData == "publisher_id":
            self.publisherId = "'" + content + "'"
        elif self.currentData == "publisher_name":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherName = "'" + content + "'"
        elif self.currentData == "publisher_zip_code":
            self.publisherZipCode = "'" + content + "'"
        elif self.currentData == "publisher_city":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherCity = "'" + content + "'"
        elif self.currentData == "publisher_state":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherState = "'" + content + "'"
        elif self.currentData == "publisher_country":
            if "'" in content:
                content = content.replace("'", "''")
            self.publisherCountry = "'" + content + "'"

#for debug
class testParseHandler(xml.sax.ContentHandler):
    def __init__(self):
        xml.sax.ContentHandler.__init__(self)
    
    # get values from file
    def endDocument(self):
        xml.sax.ContentHandler.endDocument(self)
        global sysAssId
        sysAssId += 1
        print "pass test!", sysAssId
        
#Here is connect to your PostgreSQL
#Change you database, user and port here
db = "acm"
dbUser = "minjian"
dbPort = 5433
      
conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
cur = conn.cursor()

sysAssId = 0
# these sets are used to avoid repeated information
journalIdSet = set()
authorIdSet = set()
articleIdSet = set()
publisherIdSet = set()
proceedingIdSet = set()

failedReason = set()

#start proceeding data
#count = 0
#Here is the directory of the proceeding files
os.chdir("/home/minjian/Documents/proceeding")
fileList = os.listdir('.')
for eachProceeding in fileList:
    #count += 1
    print eachProceeding
    f2 = open(eachProceeding)
    before_time = time.time()
    try:
        xml.sax.parse(f2, ACMProceedingHandler())
        #xml.sax.parse(f2, testParseHandler())
    except xml.sax._exceptions.SAXParseException as reason:
        failedReason.add(reason)
        continue
    f2.close()
    #if count == 2:
        #break

# start periodical data
#count = 0
#Here is the directory of the journal files
os.chdir("/home/minjian/Documents/periodical")
fileList = os.listdir('.')
#fileList = [1] #for debug
for eachPeriodical in fileList:
    #count += 1
    print eachPeriodical
    f1 = open(eachPeriodical)
    #f1 = open("MAG-CACM-V56I5-2447976.xml") #for debug
    before_time = time.time()
    try:
        xml.sax.parse(f1, ACMPeriodicalHandler())
        #xml.sax.parse(f1, testParseHandler())
    except xml.sax._exceptions.SAXParseException as reason:
        failedReason.add(reason)
        continue
        
    f1.close()
    #if count == 2:
        #break

cur.close()
conn.close()

#Print out information after parsing
print "-----------------"    
print len(journalIdSet)
print len(proceedingIdSet)
print len(articleIdSet)
print len(authorIdSet)
print len(publisherIdSet)
print len(failedReason)

for eachReason in failedReason:
    print eachReason