'''
This file is to show how the tables that we create in the database are related with 
the XML structure of the raw data that we got from ACM.
--------------------------------------------------------------
create table author(
AUid int not null primary key,
PersonId text not null,
AuthorProfileID bigint, 
FName text,
MName text,
LName text,
Affiliation text,
Email text);

Example in MAG-CACM-V5I10-368959.xml:
periodical
    content
        article_rec
            authors
                au
                    person_id
                    author_profile_id
                        value
                    first_name
                        value
                    middle_name
                        value
                    last_name
                        value
                    affiliation
                        value
                    email_address
                        value
--------------------------------------------------------------
#some might not have keywords

create table article(
ARid int not null primary key,
ArticleId bigint not null,
Title text,  
Publication_Date date,
JOid int,
PRid int,
foreign key (JOid) references journal (JOid),
foreign key (PRid) references proceeding (PRid));

Example in MAG-CACM-V5I10-368959.xml:
periodical
    journal_rec
        journal_id: J79
    content
        article_rec
            article_id: 368962
            article_publication_date: 10-01-1962
            title
                value: Official Notices:Leaders for 1963 SJCC picked
            cited_by
                cited_by_object_id: 512286

--------------------------------------------------------------
create table journal(
JOid int not null primary key,
JournalId text not null,
Name text,
Periodical_Type text,
Publication_Date date,
PUid int,
foreign key (PUid) references publisher (PUid));

Example in MAG-CACM-V5I10-368959.xml:
periodical
    journal_rec
        journal_id: J79
        journal_name: Communication of the ACM
        periodical_type: magazine
        publisher
            publisher_id: PUB27
    issue_rec
        issue_id: 368959
        publication_date: 10-01-1962
--------------------------------------------------------------
        
create table publisher(
PUid int not null primary key,
PublisherId text not null,
Name text,
ZipCode text,
City text,
State text,
Country text);

Example in MAG-CACM-V5I10-368959.xml:
periodical
    journal_rec
        journal_id: J79
        publisher
            publisher_id: PUB27
            publisher_name: ACM
            publisher_city: New York
            publisher_state: NY
            publisher_country: USA
            publisher_zip_code: 10121-0701
--------------------------------------------------------------
            
create table proceeding(
PRid int not null primary key,
ProceedingId bigint not null,
Title text,
Subtitle text,
Proc_Desc text,
Con_City text,
Con_State text,
Con_Country text,
Con_Start_Date date,
Con_End_Date date,
Publication_Date date,
PUid int,
foreign key (PUid) references publisher (PUid));

Example in MAG-CACM-V5I10-368959.xml:
proceeding
    conference_rec
        conference_date
            start_date: 04/10/2010
            end_date: 04/15/2010
        conference_loc
            city
                value: Atlanta
            state: Georgia
            country: USA
    proceeding_rec
        proc_id: 1753326
        proc_desc: Proceeding of the SIGCHI Conference
        proc_title: Human Factors in Computing Systems
        publication_date: 04-10-2010
        publisher
            publisher_id: PUB27

--------------------------------------------------------------
create table writes(
AUid int not null,
ARid int not null,
primary key (AUid, ARid),
foreign key (AUid) references AUTHOR (AUid),
foreign key (ARid) references ARTICLE (ARid));

Example in MAG-CACM-V56I5-2447976.xml:
periodical
    content
        section
            article_rec
                article_id
                authors
                    au
                        person_id
            
        article_rec
            article_id
            authors
                au
                    person_id
                    
--------------------------------------------------------------
#some article_rec doesn't have cited_by_list & references

create table cites(
ARid int not null,
CitedARid int not null,
primary key (ARid, CitedARid),
foreign key (ARid) references ARTICLE (ARid),
foreign key (CitedARid) references ARTICLE (ARid));

Example in MAG-CACM-V56I5-2447976.xml:
periodical
    content
        section
            article_rec
                article_id
                references
                    ref
                        ref_obj_id
                        ref_obj_pid
                        ref_text
                cited_by_list
                    cited_by
                        cited_by_object_id
        article_rec
            article_id
            references
                ref
                    ref_obj_id
                    ref_obj_pid
                    ref_text
            cited_by_list
                cited_by
                    cited_by_object_id
                    
Example in PROC-3DOR09-2009-2381128.xml
proceeding
    content
        section
            article_rec
                article_id
                references
                    ref
                        ref_obj_id
                        ref_obj_pid
                        ref_text
                cited_by_list
                    cited_by
                        cited_by_object_id
                
@author: minjian
'''
